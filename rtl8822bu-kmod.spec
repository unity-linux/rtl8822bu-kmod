# Define the kmod package name here.
%define kmod_name rtl8822bu
%define rmod_name 88x2bu

# If kversion isn't defined on the rpmbuild line, define it here.
%{!?kversion: %define kversion %(uname -r)}

Name:    %{kmod_name}-kmod
Version: 20181226.%(echo %{kversion}|cut -d- -f1)
Release: 0.1%{?dist}
Group:   System Environment/Kernel
Summary: %{kmod_name} kernel module(s)
License:    GPL
URL:        https://github.com/MeissnerEffect/rtl8822bu
Source0:    https://github.com/MeissnerEffect/rtl8822bu/archive/master.zip

BuildRequires: kernel-devel = %(echo %{kversion}|cut -d- -f1)
BuildRequires: kernel-core = %(echo %{kversion}|cut -d- -f1)
BuildRequires: elfutils-devel
BuildRequires: perl
BuildRequires: kmodtool
BuildRequires: bc
BuildRequires: bsdtar
BuildRequires: redhat-rpm-config
ExclusiveArch: x86_64 i686 armv7hl

# Magic hidden here.
%{expand:%(kmodtool rpmtemplate %{kmod_name} %{kversion} "")}

# Disable the building of the debug package(s).
%define debug_package %{nil}

%description
This package provides the %{kmod_name} kernel module(s).
It is built to depend upon the specific ABI provided by a range of releases
of the same variant of the Linux kernel and not on any one specific build.

%prep
#%setup -q -n %{kmod_name}-%{version}
%setup -q -c -T
bsdtar -xf %{SOURCE0} -s'|[^/]*/||'
echo "override %{kmod_name} * weak-updates/%{kmod_name}" > kmod-%{kmod_name}.conf

%build
#KSRC=%{_usrsrc}/kernels/%{kversion}
#%{__make} -C "${KSRC}" %{?_smp_mflags} modules M=$PWD modules
%{__make} %{?_smp_mflags} modules M=$PWD modules

%install
%{__install} -d %{buildroot}/lib/modules/%{kversion}/extra/%{rmod_name}/
%{__install} %{rmod_name}.ko %{buildroot}/lib/modules/%{kversion}/extra/%{rmod_name}/
%{__install} -d %{buildroot}%{_sysconfdir}/depmod.d/

# strip the modules(s)
find %{buildroot} -type f -name \*.ko -exec %{__strip} --strip-debug \{\} \;

# Sign the modules(s)
%if %{?_with_modsign:1}%{!?_with_modsign:0}
# If the module signing keys are not defined, define them here.
%{!?privkey: %define privkey %{_sysconfdir}/pki/SECURE-BOOT-KEY.priv}
%{!?pubkey: %define pubkey %{_sysconfdir}/pki/SECURE-BOOT-KEY.der}
for module in $(find %{buildroot} -type f -name \*.ko);
do %{__perl} /usr/src/kernels/%{kversion}/scripts/sign-file \
sha256 %{privkey} %{pubkey} $module;
done
%endif

%clean
%{__rm} -rf %{buildroot}

%files
/lib/modules/%{kversion}/extra/%{rmod_name}/%{rmod_name}.ko

%changelog
* Fri Jan 04 2019 JMiahMan <jmiahman@unity-linux.org> - 20181226-0.1
- Initial build
